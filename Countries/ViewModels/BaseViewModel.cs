﻿namespace Countries.ViewModels
{
    using Prism.AppModel;
    using Prism.Mvvm;
    using Prism.Navigation;

    public class BaseViewModel : BindableBase, IInitialize, INavigatedAware, IPageLifecycleAware
    {
        public INavigationService NavigationService;

        string title;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        public BaseViewModel(INavigationService navigationService)
        {
            NavigationService = navigationService;
        }

        public virtual void Initialize(INavigationParameters parameters) { }

        public virtual void OnNavigatedFrom(INavigationParameters parameters) { }

        public virtual void OnNavigatedTo(INavigationParameters parameters) { }

        public virtual void OnAppearing() { }

        public virtual void OnDisappearing() { }
    }
}