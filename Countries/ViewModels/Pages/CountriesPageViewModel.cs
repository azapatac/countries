﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Countries.Models;
using Newtonsoft.Json;
using Prism.Navigation;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Countries.ViewModels.Pages
{
    public class CountriesPageViewModel : BaseViewModel
    {
        const string urlBase = "https://restcountries.eu/rest/v2/";

        private IList<CountryModel> countries;
        public IList<CountryModel> Countries
        {
            get { return countries; }
            set { SetProperty(ref countries, value); }
        }

        bool isRunning;
        public bool IsRunning
        {
            get { return isRunning; }
            set { SetProperty(ref isRunning, value); }
        }

        public ICommand LoadCountriesCommand { get; set; }

        public CountriesPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            LoadCountriesCommand = new Command(async () => { await LoadCountries(); });
        }

        public override void Initialize(INavigationParameters parameters)
        {
            base.Initialize(parameters);
            Title = parameters.GetValue<string>("title");
        }

        private async Task LoadCountries()
        {
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {
                await App.Current.MainPage.DisplayAlert("Countries", "No Internet", "OK");
                return;
            }

            try
            {
                IsRunning = true;

                HttpClient client = new HttpClient();

                string content = await client.GetStringAsync(urlBase);

                List<CountryModel> data = JsonConvert.DeserializeObject<List<CountryModel>>(content);

                Countries = new ObservableCollection<CountryModel>(data);                
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("DataBase", ex.Message, "OK");
            }
            finally
            {
                IsRunning = false;
            }
        }
    }
}