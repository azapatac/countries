﻿namespace Countries
{
    using Countries.ViewModels.Pages;
    using Countries.Views.Pages;
    using Prism;
    using Prism.Ioc;
    using Prism.Navigation;
    using Xamarin.Forms;

    public partial class App
    {        
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected async override void OnInitialized()
        {
            Device.SetFlags(new[] { "Brush_Experimental" });

            INavigationParameters keys = new NavigationParameters();

            keys.Add("title", ".Net University");

            await NavigationService.NavigateAsync("/NavigationPage/Countries", keys);
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<CountriesPage, CountriesPageViewModel>("Countries");
        }        
    }
}